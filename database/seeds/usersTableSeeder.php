<?php

use Illuminate\Database\Seeder;

class usersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'jack',
                        'email' => 'jack@gmail.com',
                        'password' => '12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'ner',
                        'email' => 'ner@gmail.com',
                        'password' => '12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
              
            
                    ]);
    }

}
