<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::get('/hello', function () {
    return "Hello world!" ;
});

// Route::get('/student/{id}', function ($id) {
//     return "Hello student - ".$id ;
// });

Route::get('/student/{id?}', function ($id = 'no student provided') {
    return "Hello student - ".$id ;
});

Route::get('/comment/{id?}', function ($id = 'no comment provided' ) {
    return view('comment',['id'=>$id]);
});

Route::resource('todos', 'TodoController');